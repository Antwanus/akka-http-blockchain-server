package streaming;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.stream.*;
import akka.stream.javadsl.*;
import akka.stream.typed.javadsl.ActorFlow;
import blockchain.ManagerBehavior;
import blockchain.MiningSystemBehavior;
import model.Block;
import model.BlockChain;
import model.HashResult;
import model.Transaction;

import java.sql.SQLOutput;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletionStage;

public class Main {
    private static int txId = -1;
    private static Random random = new Random();

    public static void main(String[] args) {

        BlockChain blockChain = new BlockChain();

        ActorSystem<ManagerBehavior.Command> actorSys = ActorSystem.create(
                MiningSystemBehavior.create(), "minerSys");

        Source<Transaction, NotUsed> txSource = Source.repeat(1).throttle(1, Duration.ofSeconds(1))
            .map( e -> {
                System.out.println("Received TX - ID: " + ++txId);
                return new Transaction(
                        txId, System.currentTimeMillis(),
                        random.nextInt(1000), random.nextDouble()*100
                );
            });

        /** while mining a block, collect new transactions (from txSource) in a new block.
         *  when previous block is mined, start mining the current block & repeat */
        Flow<Transaction, Block, NotUsed> blockBuilder = Flow.of(Transaction.class)
                .map(tx -> {
                    List<Transaction> list = new ArrayList<>();
                    list.add(tx);
                    Block newBlock = new Block(blockChain.getLastHash(), list);
                    System.out.println("NEW BLOCK: " + newBlock.toString());
                    return newBlock;
        // we don't need to use .conflateWithSeed because we're receiving a block from .map()
        }).conflate( (accumulatorBlock, newBlock) -> {
            accumulatorBlock.addTransactionToList(newBlock.getFirstTransaction());
            System.out.println("Conflating block: " + accumulatorBlock);
            return accumulatorBlock;
        });

        Flow<Block, HashResult, NotUsed> minerFlow = ActorFlow.ask(
                actorSys,                       // ref
                Duration.ofSeconds(30),         // timeout
                (block, hashResultActorRef) ->  // makeMsg
                    new ManagerBehavior.MineBlockCommand(block, hashResultActorRef, 5)
        );

        Source<String, NotUsed> firstHashValue = Source.single("0");

        Flow<Block, Block, NotUsed> miningFlow = Flow.fromGraph(
                GraphDSL.create(builder -> {
                    UniformFanInShape<String, String> fanInResultHash = builder.add(Merge.create(2));
                    FanInShape2<String, Block, Block> zipResultHashAndBlock = builder.add(
                            ZipWith.create( (hash, block) -> {
                                return new Block(hash, block.extractTransactions());
                            })
                    );
                    UniformFanOutShape<Block, Block> broadcastBlock = builder.add(Broadcast.create(2));
                    FlowShape<Block, HashResult> mineBlock = builder.add(minerFlow);

                    UniformFanOutShape<HashResult, HashResult> broadcastHashResult = builder.add(Broadcast.create(2));

                    FanInShape2<Block, HashResult, Block> blockAndHashFanIn = builder.add(
                        ZipWith.create( (block, hashResult) -> {
                            block.setHash(hashResult.getHash());
                            block.setNonce(hashResult.getNonce());
                            return block;
                        }));

                    //todo connect inlet_2
                    builder.from(builder.add(firstHashValue)).viaFanIn(fanInResultHash);

                    builder.from(fanInResultHash).toInlet(zipResultHashAndBlock.in0());
                    builder.from(zipResultHashAndBlock.out()).viaFanOut(broadcastBlock);

                    builder.from(broadcastBlock)
                            .toInlet(blockAndHashFanIn.in0());

                    builder.from(broadcastBlock)
                                .via(mineBlock)
                                .viaFanOut(broadcastHashResult)
                            .toInlet(blockAndHashFanIn.in1());
                    builder.from(broadcastHashResult)
                            .via(builder.add(Flow.of(HashResult.class).map(hr -> hr.getHash())))
                            .viaFanIn(fanInResultHash);
                    return FlowShape.of(zipResultHashAndBlock.in1(), blockAndHashFanIn.out());
                })
        );

        Sink<Block, CompletionStage<Done>> sink = Sink.foreach(block -> {
           blockChain.addBlock(block);
           blockChain.printAndValidate();
        });

        txSource.via(blockBuilder)
                 .via(
                         miningFlow.async()
                                 .addAttributes(Attributes.inputBuffer(1, 1))
                 )
            .to(sink)
            .run(actorSys);


    }


}
